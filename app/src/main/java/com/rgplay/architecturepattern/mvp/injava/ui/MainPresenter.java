package com.rgplay.architecturepattern.mvp.injava.ui;

import com.rgplay.architecturepattern.mvp.injava.model.ComputerLanguage;

public class MainPresenter {
    IMainView iMainView;

    // Link between Presenter and Activity
    // Note that the Activity will implement the Interface and it gains the behavior of the interface
    public MainPresenter(IMainView iMainView){
        this.iMainView = iMainView;
    }

    // Linking Presenter with Model
    // This method will be called from Presenter itself, but, with from the method linked with Activity event
    public ComputerLanguage getComputerLanguageModel(){
        return new ComputerLanguage("Java","Android");
    }

    // Linking Presenter with Activity
    // This method will be called from Activity on trigger of an event (ex: onClick), which in turn has an method implemented inside the activity
    // The method overridden inside the activity will set the values of the views
    public void getComputerLanguage() {
        iMainView.getComputerLanguage(getComputerLanguageModel().getLanguage());
    }
}
