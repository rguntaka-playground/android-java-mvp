package com.rgplay.architecturepattern.mvp.injava.ui;

/*
 This interface will be implemented inside the Activity, thus, the activity gains the behavior of this View Interface
 Note that, the Activity is a View of this MVP architecture
 */
public interface IMainView {
    void getComputerLanguage(String mobilePlatform);
}
