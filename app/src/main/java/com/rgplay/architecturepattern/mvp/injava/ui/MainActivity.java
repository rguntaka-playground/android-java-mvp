package com.rgplay.architecturepattern.mvp.injava.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.rgplay.architecturepattern.mvp.injava.R;

public class MainActivity extends AppCompatActivity implements IMainView{

    TextView tvDisplayArea;
    Button btClickMeToDisplay;

    // linking Activity with Presenter
    MainPresenter mainPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvDisplayArea = findViewById(R.id.tvDisplayArea);
        btClickMeToDisplay = findViewById(R.id.btClickMeToDisplay);

        mainPresenter = new MainPresenter(this);

        btClickMeToDisplay.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        // Calling the Presenter to fetch data from DB
                        // No Link between Activity & Model, only via Presenter
                        // Activity is handling only the UI
                        mainPresenter.getComputerLanguage();
                    }
                }
        );
    }

    // This overridden method will be invoked from the Presenter after fetching required data from DB
    // Btw, this method will be triggered in sequence after the event trigger
    @Override
    public void getComputerLanguage(String mobilePlatform) {
        tvDisplayArea.setText(mobilePlatform);
    }
}